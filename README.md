# otel-demo

A Kafka producer and consumer demonstration with Jaeger and Instana exporters.

## Prerequisites

These instructions expect you have [docker-compose](https://docs.docker.com/compose/) installed.

## Kafka, zookeeper, jaeger
Launch `Kafka`, `ZooKeeper` and `Jaeger` containers in order to run this example:

```sh
$ docker-compose up -d zoo kafka jaeger
```

## Producer

Run the following commands in order to produce a message:

```sh
$ docker-compose up producer
```

## Consumer

Run the following commands in order to consume a message:

```sh
$ docker-compose up consumer
```

## Jaeger

to access the Jaeger UI, use a brower and go to `localhost:16686`

## Cleanup

Once you've finished testing these examples, don't forger to cleanup your environment by removing docker containers:

```sh
$ docker-compose down
```
