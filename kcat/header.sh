#
set -x
kcat -b localhost:9092 -t kafka-v2-tests -C -e -f '
  Key (%K bytes): %k
  Value (%S bytes): %s
  Timestamp: %T
  Partition: %p
  Offset: %o
  Headers: %h\n'
